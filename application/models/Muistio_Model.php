<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muistio_Model extends CI_Model {
        public function __construct() {
                parent::__construct();
        }
    
        public function hae_kaikki($asiakas) {
            $this->db->get('asiakas_id', $asiakas);
            $query = $this->db->get('muistio');
            return $query->result();
        }
        
        public function lisaa($data) {
            $this->db->insert('muistio',$data);
            return $this->db->insert_id();
        }
        
        public function poista($kayttaja) {
            $this->db->where('id',$kayttaja);
            $this->db->delete('muistio');
        }
        
        public function muokkaa($data) {
            $this->db->where('id',$data['id']);
            $this->db->update('muistio',$data);
        }
        
        public function hae($kayttaja) {
            $this->db->where('id',$kayttaja);
            $query = $this->db->get('muistio');
            return $query->row();
        }
}
