<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muistio extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('muistio_model');
        $this->load->library('util');
    }
    
    public function index($asiakas) {
        $this->session->set_userdata('asiakas_id', $asiakas);
        $data['aika'] = date('d.m.Y H.i', time());
        $data['main_content'] = 'muistio_view';
        $data['asiakkaat'] = $this->asiakas_model->hae_kaikki($asiakas);
        $this->load->view('template',$data);
    }
    
    public function tallenna() {
        $data = array(
            'tallennettu' => $this->util->format_fin_to_sqldate($this->input->post('aika')),
            'teksti' => $this->input->post('teksti'),
            'asiakas_id' => $this->session->userdata('asiakas_id')
        );
        $this->muistio_model->lisaa($data);
        redirect('muistio/index/' . $this->session->userdata('asiakas_id'));
    }
    
    public function poista() {
        
    }
}