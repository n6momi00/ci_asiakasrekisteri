<div class="row">
    <div class="col-xs-12 col-md-offset-4 col-md-4">
        <h3>Rekisteröidy</h3>
        <?php print validation_errors(); ?>
        <form role="form" method="post" action="<?php print site_url(). '/kayttaja/rekisteroidy'?>">
            <div class="form-group">
                <label for="tunnus">Käyttäjätunnus:</label>
                <input type="email" class="form-control" name="tunnus">
            </div>
            <div class="form-group">
                <label for="salasana">Salasana:</label>
                <input type="password" class="form-control" name="salasana">
            </div>
            <div class="form-group">
                <label for="salasana2">Salasana uudestaan:</label>
                <input type="password" class="form-control" name="salasana2">
            </div>
            <button class="btn btn-primary">Tallenna</button>&nbsp;
            <a class="btn btn-default" href="<?php print site_url(). '/kayttaja' ?>">Kirjaudu</a>
        </form>
    </div>    
</div>
