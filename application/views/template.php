<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Asiakasrekisteri</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php print base_url() . '/css/style.css';?>">
    </head>
    <body>
        
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
<!--                <a class="navbar-brand" href="#">WebSiteName</a>-->
            </div>
            <ul class="nav navbar-nav navbar-right">
                <?php if ($this->session->has_userdata('kayttaja')) { ?>
                    <li><a href="<?php print site_url(). 'kayttaja/kirjaudu_ulos' ?>"><span class="glyphicon glyphicon-log-in"></span> Kirjaudu ulos</a></li>
                <?php } ?>
            </ul>
        </div>
    </nav>
        
        <div class="container">
            <h1>Asiakasrekisteri</h1>
            <?php
            $this->load->view($main_content);
            ?>
        </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php print base_url() . '/js/function.js'; ?>"></script>
    </body>
</html>
