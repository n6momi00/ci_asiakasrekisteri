    <form action="<?php print site_url() . 'asiakas/index'; ?>" method="post" class="search-form">
    <div class="form-group has-feedback">
            <label for="search" class="sr-only">Search</label>
            <input type="text" class="form-control" name="search" id="search" placeholder="Search">
            <span class="glyphicon glyphicon-search form-control-feedback"></span>
    </div>
</form>
<h3>Asiakas<?php //$this->lang->line('asiakkaat'); ?></h3>
<?php print anchor('asiakas/lisaa', 'Lisää'); ?>
<table class="table">
    <tr>
        <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/id", "ID"); ?></th>
        <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/sukunimi", "Sukunimi"); ?></th>
        <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/etunimi", "Etunimi"); ?></th>
        <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/lahiosoite", "Lähiosoite"); ?></th>
        <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/postitoimipaikka", "Postitoimipaikka"); ?></th>
        <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/postinumero", "Postinumero"); ?></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    <?php
    foreach ($asiakkaat as $asiakas) {
        print "<tr>";
        print "<td>$asiakas->id</td>";
        print "<td>$asiakas->sukunimi</td>";
        print "<td>$asiakas->etunimi</td>";
        print "<td>$asiakas->lahiosoite</td>";
        print "<td>$asiakas->postitoimipaikka</td>";
        print "<td>$asiakas->postinumero</td>";
        print "<td>" . anchor("asiakas/poista/$asiakas->id", "Poista") . "</td>";
        print "<td>" . anchor("asiakas/muokkaa/$asiakas->id", "Muokkaa") . "</td>";
        print "<td>" . anchor("muistio/index/$asiakas->id", "Muistio") . "</td>";
        print "</tr>";
    }
    ?>
</table>
<?php echo $this->pagination->create_links(); ?>