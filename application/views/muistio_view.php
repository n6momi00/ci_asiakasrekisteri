<div class="row">
    <div class="col-xs-12 col-md-offset-4 col-md-4">
        <form role="form" method="post" action="<?php print site_url() . 'muistio/tallenna'; ?>">
            <h3></h3>
            <div class="form-group">
                <label for="aika">Aika:</label>
                <input type="datetime" class="form-control" name="aika" value="<?php print $aika; ?>">
            </div>
            <div class="form-group">
                <label for="teksti">Teksti:</label>
                <input type="text" class="form-control" name="teksti">
            </div>
            <div>
                <a class="btn btn-default" href="<?php print site_url() . 'asiakas/index'; ?>">Etusivulle</a>
                <button class="btn btn-primary">Tallenna</button>
            </div>
        </form>
        <div>
            <table>
                <?php
                foreach ($muistiot as $muistio) {
                    print "<tr>";
                    print "<td>$muistio->tallennettu</td>";
                    print "<td>$muistio->teksti</td>";
                    print "</tr>";
                }
                ?>
            </table>
        </div>
    </div>
</div>