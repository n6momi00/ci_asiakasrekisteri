$(function() {
    $("#search").keypress(function(e) {
        if(e.which === 13) {
            $(this).parents("form:first").submit();
        }
    });
});