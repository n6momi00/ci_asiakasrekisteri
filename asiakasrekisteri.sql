/**/
drop database if exists asiakasrekisteri;

create database asiakasrekisteri;

use asiakasrekisteri;

create table asiakas(
    id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    lahiosoite varchar(50),
    postinumero varchar(5),
    postitoimipaikka varchar(50)
);

create table kayttaja(
    id int primary key auto_increment,
    email varchar(100) not null unique,
    salasana varchar(255) not null
);

create table muistio(
    id int primary key auto_increment,
    teksti text not null,
    asiakas_id int not null,
    index (asiakas_id),
    foreign key (asiakas_id) references asiakas(id) on delete restrict,
    tallennettu timestamp default current_timestamp
);

insert into asiakas (etunimi, sukunimi) value ('Matti', 'Meikäläinen');
insert into asiakas (etunimi, sukunimi) value ('Jaska', 'Jokunen');
